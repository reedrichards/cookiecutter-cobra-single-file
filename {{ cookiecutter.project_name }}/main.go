package main

import (
	_ "embed"

	"log"
	"net/http"
)
//go:embed README.md
var readme string

func main() {
	http.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("OK"))
		if err != nil {
			log.Println(err)
		}
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write( []byte(readme))
		if err != nil {
			log.Println(err)
		}
	})
	log.Println("server listening")
	http.ListenAndServe(":8000", nil)
}
